from django.db import models

# Create your models here.

class ServRestUser(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=30)
    administrator = models.BooleanField(default=False)
    logged = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class ServRestProdutos(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    description = models.CharField(max_length=100, default='Sem descrição')
    quantity = models.IntegerField()

    def __str__(self):
        return self.name


class ServRestCarrinhos(models.Model):
    owner = models.ForeignKey(ServRestUser, on_delete=models.CASCADE, default=None)
    quantity = models.IntegerField()
    products = models.ForeignKey(ServRestProdutos, on_delete=models.CASCADE)
