from rest_framework import viewsets
from user.models import ServRestUser, ServRestProdutos, ServRestCarrinhos
from user.api.v1.serializer import ServRestUserSerializer, ServRestProdutosSerializer, ServRestCarrinhoSerializer

class ServRestUserViewSet(viewsets.ModelViewSet):
    """Exibir todos os usuarios"""
    queryset = ServRestUser.objects.all()
    serializer_class = ServRestUserSerializer


class ServRestProdutosViewSet(viewsets.ModelViewSet):
    """Exibir todos os produtos"""
    queryset = ServRestProdutos.objects.all()
    serializer_class = ServRestProdutosSerializer


class ServRestCarrinhoViewSet(viewsets.ModelViewSet):
    queryset = ServRestCarrinhos.objects.all()
    serializer_class = ServRestCarrinhoSerializer
