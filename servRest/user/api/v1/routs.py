from django.urls import path

from rest_framework.routers import DefaultRouter
from user.api.v1.views import ServRestUserViewSet, ServRestProdutosViewSet, ServRestCarrinhoViewSet

# Criando uma rota e registrando os viewSets nele
router = DefaultRouter()
router.register(r'users', ServRestUserViewSet, basename="Users")
router.register(r'products', ServRestProdutosViewSet, basename="Products")
router.register('carrinhos', ServRestCarrinhoViewSet, basename="Carrinho")
