from rest_framework import serializers
from user.models import ServRestUser, ServRestProdutos, ServRestCarrinhos
from user.validators import Validator

class ServRestUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServRestUser
        fields = [
            'id',
            'name',
            'email',
            'password',
            'administrator'
        ]
    def validate(self, data):
        if Validator.email_is_valid(data['email']):
            raise serializers.ValidationError({"email" : "email invalido, email já cadastrado!"})
        return data


class ServRestProdutosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServRestProdutos
        fields = [
            'id',
            "name",
            "price",
            "description",
            "quantity"
        ]

    def validate(self, data):
        
        if Validator.name_is_valid_products(data['name']):
            raise serializers.ValidationError({"name" : "Nome invalido"})

        if Validator.negative_fields(data['price']):
            raise serializers.ValidationError({"price" : "Preço invalido"})

        if Validator.negative_fields(data['quantity']):
            raise serializers.ValidationError({"quantity" : "Quantidade invalida"})     
        
        return data


class ServRestCarrinhoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServRestCarrinhos
        fields = [
            'id',
            'owner',
            'products',
            'quantity'
        ]

    def validate(self, data):
        if Validator.negative_fields(data['quantity']):
            raise serializers.ValidationError({"quantity" : "Quantidade invalida"})
        return data
