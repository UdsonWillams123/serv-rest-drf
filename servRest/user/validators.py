from user.models import ServRestProdutos, ServRestUser
class Validator():

    def negative_fields(price):
        if price < 0:
            return True
        return False

    def email_is_valid(email):
        try:
            bd = ServRestUser.objects.get(email=email)
        except:
            bd = 1
        if bd == 1:
            return False
        return True

    def name_is_valid_products(name):
        if name.isalpha():
            try:
                bd = ServRestProdutos.objects.get(name=name)
            except:
                bd = 1
            if bd == 1:
                return False
            return True
